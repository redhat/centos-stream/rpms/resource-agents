#
# All modifications and additions to the file contributed by third parties
# remain the property of their copyright owners, unless otherwise agreed
# upon. The license for this file, and modifications and additions to the
# file, is the same license as for the pristine package itself (unless the
# license for the pristine package is not an Open Source License, in which
# case the license is the MIT License). An "Open Source License" is a
# license that conforms to the Open Source Definition (Version 1.9)
# published by the Open Source Initiative.
#

# Below is the script used to generate a new source file
# from the resource-agent upstream git repo.
#
# TAG=$(git log --pretty="format:%h" -n 1)
# distdir="ClusterLabs-resource-agents-${TAG}"
# TARFILE="${distdir}.tar.gz"
# rm -rf $TARFILE $distdir
# git archive --prefix=$distdir/ HEAD | gzip > $TARFILE
#

%global upstream_prefix ClusterLabs-resource-agents
%global upstream_version 56e76b01

# Whether this platform defaults to using systemd as an init system
# (needs to be evaluated prior to BuildRequires being enumerated and
# installed as it's intended to conditionally select some of these, and
# for that there are only few indicators with varying reliability:
# - presence of systemd-defined macros (when building in a full-fledged
#   environment, which is not the case with ordinary mock-based builds)
# - systemd-aware rpm as manifested with the presence of particular
#   macro (rpm itself will trivially always be present when building)
# - existence of /usr/lib/os-release file, which is something heavily
#   propagated by systemd project
# - when not good enough, there's always a possibility to check
#   particular distro-specific macros (incl. version comparison)
%define systemd_native (%{?_unitdir:1}%{!?_unitdir:0}%{nil \
  } || %{?__transaction_systemd_inhibit:1}%{!?__transaction_systemd_inhibit:0}%{nil \
  } || %(test -f /usr/lib/os-release; test $? -ne 0; echo $?))

# determine the ras-set to process based on configure invokation
%bcond_with rgmanager
%bcond_without linuxha

Name:		resource-agents
Summary:	Open Source HA Reusable Cluster Resource Scripts
Version:	4.16.0
Release:	9%{?rcver:%{rcver}}%{?numcomm:.%{numcomm}}%{?alphatag:.%{alphatag}}%{?dirty:.%{dirty}}%{?dist}
License:	GPL-2.0-or-later AND LGPL-2.1-or-later
URL:		https://github.com/ClusterLabs/resource-agents
Source0:	%{upstream_prefix}-%{upstream_version}.tar.gz
Patch0: 	pgsqlms-ra.patch
Patch1: 	RHEL-66293-1-aws-agents-reuse-imds-token-until-it-expires.patch
Patch2: 	RHEL-66293-2-aws-agents-reuse-imds-token-improvements.patch
Patch3: 	RHEL-68740-awsvip-add-interface-parameter.patch
Patch4: 	RHEL-72954-1-openstack-cinder-volume-wait-for-volume-to-be-available.patch
Patch5: 	RHEL-72954-2-openstack-cinder-volume-fix-detach-not-working-during-start-action.patch
Patch6: 	RHEL-75574-1-all-agents-use-grep-E-F.patch
Patch7: 	RHEL-75574-2-ocf-binaries-add-FGREP.patch
Patch8: 	RHEL-76007-Filesystem-dont-report-warnings-when-creating-resource.patch
Patch9: 	RHEL-76037-1-storage-mon-remove-unused-variables.patch
Patch10: 	RHEL-76037-2-storage-mon-fix-daemon-mode-bug-that-caused-delayed-initial-score.patch
Patch11: 	RHEL-76037-3-storage-mon-only-use-underscores-in-functions.patch
Patch12: 	RHEL-76037-4-storage-mon-check-if-daemon-is-already-running.patch
Patch13: 	RHEL-76037-5-storage-mon-log-storage_mon-is-already-running-in-start-action.patch
Patch14: 	RHEL-73689-1-ocf-shellfuncs-fix-syntax-error-in-crm_mon_no_validation.patch
Patch15: 	RHEL-73689-2-ocf-shellfuncs-add-missing-variable-in-crm_mon_no_validation.patch
Patch16:	RHEL-79822-1-portblock-fix-version-detection.patch
Patch17:	RHEL-79822-2-portblock-use-ocf_log-for-logging.patch

# bundled ha-cloud-support libs
Patch500:	ha-cloud-support-aliyun.patch
Patch501:	ha-cloud-support-gcloud.patch

Obsoletes:	heartbeat-resources <= %{version}
Provides:	heartbeat-resources = %{version}

# Build dependencies
BuildRequires: make
BuildRequires: automake autoconf pkgconfig gcc
BuildRequires: perl
BuildRequires: libxslt glib2-devel libqb-devel
BuildRequires: systemd
BuildRequires: which

%if 0%{?fedora} || 0%{?centos} > 7 || 0%{?rhel} > 7 || 0%{?suse_version}
BuildRequires: python3-devel
%else
BuildRequires: python-devel
%endif

# for pgsqlms
BuildRequires: perl-devel perl-English perl-FindBin

%ifarch x86_64
BuildRequires: ha-cloud-support
%endif

%if 0%{?fedora} || 0%{?centos} || 0%{?rhel}
BuildRequires: docbook-style-xsl docbook-dtds
%if 0%{?rhel} == 0
BuildRequires: libnet-devel
%endif
%endif

%if 0%{?suse_version}
BuildRequires:  libnet-devel
%if 0%{?suse_version} > 1500
BuildRequires:  cluster-glue-devel
%else
BuildRequires:  libglue-devel
%endif
BuildRequires:  libxslt docbook_4 docbook-xsl-stylesheets
%endif

# dependencies for powervs-subnet
BuildRequires: python3-requests python3-urllib3

## Runtime deps
# system tools shared by several agents
Requires: /bin/bash /usr/bin/grep /bin/sed /bin/gawk
Requires: /bin/ps /usr/bin/pkill /usr/bin/hostname /usr/bin/netstat
Requires: /bin/mount
%if 0%{?suse_version}
Requires: /usr/bin/fuser
%else
Requires: /usr/sbin/fuser
%endif

# Filesystem / fs.sh / netfs.sh
%if 0%{?fedora} > 39 || 0%{?rhel} > 9 || 0%{?suse_version}
Requires: /usr/sbin/fsck
%else
Requires: /sbin/fsck
%endif
Requires: /usr/sbin/fsck.ext2 /usr/sbin/fsck.ext3 /usr/sbin/fsck.ext4
Requires: /usr/sbin/fsck.xfs
%if 0%{?fedora} > 40 || 0%{?rhel} > 9 || 0%{?suse_version}
Requires: /usr/sbin/mount.nfs /usr/sbin/mount.nfs4
%else
Requires: /sbin/mount.nfs /sbin/mount.nfs4
%endif
%if (0%{?fedora} && 0%{?fedora} < 33) || (0%{?rhel} && 0%{?rhel} < 9) || (0%{?centos} && 0%{?centos} < 9) || 0%{?suse_version}
%if (0%{?rhel} && 0%{?rhel} < 8) || (0%{?centos} && 0%{?centos} < 8)
Requires: /usr/sbin/mount.cifs
%else
Recommends: /usr/sbin/mount.cifs
%endif
%endif

# IPaddr2
Requires: /sbin/ip

# LVM / lvm.sh
Requires: /usr/sbin/lvm

# nfsserver / netfs.sh
%if 0%{?fedora} > 40 || 0%{?rhel} > 9 || 0%{?suse_version}
Requires: /usr/sbin/rpc.statd
%else
Requires: /sbin/rpc.statd
%endif
Requires: /usr/sbin/rpc.nfsd /usr/sbin/rpc.mountd

# ocf.py
Requires: python3

# ethmonitor
Requires: bc

# rgmanager
%if %{with rgmanager}
# ip.sh
Requires: /usr/sbin/ethtool
Requires: /sbin/rdisc /usr/sbin/arping /bin/ping /bin/ping6

# nfsexport.sh
%if 0%{?fedora} > 39 || 0%{?rhel} > 9
Requires: /usr/sbin/findfs
Requires: /usr/sbin/quotaon /usr/sbin/quotacheck
%else
Requires: /sbin/findfs
Requires: /sbin/quotaon /sbin/quotacheck
%endif
%endif

%description
A set of scripts to interface with several services to operate in a
High Availability environment for both Pacemaker and rgmanager
service managers.

%ifarch x86_64
%package cloud
License:	GPLv2+ and LGPLv2+
Summary:	Cloud resource agents
Requires:	%{name} = %{version}-%{release}
Requires:	ha-cloud-support
Requires:	python3-requests python3-urllib3
Requires:	socat
Provides:	resource-agents-aliyun
Obsoletes:	resource-agents-aliyun <= %{version}
Provides:	resource-agents-gcp
Obsoletes:	resource-agents-gcp <= %{version}

%description cloud
Cloud resource agents allows Cloud instances to be managed
in a cluster environment.
%endif

%package paf
License:	PostgreSQL
Summary:	PostgreSQL Automatic Failover (PAF) resource agent
Requires:	%{name} = %{version}-%{release}
Requires:	perl-interpreter perl-lib perl-English perl-FindBin

%description paf
PostgreSQL Automatic Failover (PAF) resource agents allows PostgreSQL
databases to be managed in a cluster environment.

%prep
%if 0%{?suse_version} == 0 && 0%{?fedora} == 0 && 0%{?centos} == 0 && 0%{?rhel} == 0
%{error:Unable to determine the distribution/version. This is generally caused by missing /etc/rpm/macros.dist. Please install the correct build packages or define the required macros manually.}
exit 1
%endif
%setup -q -n %{upstream_prefix}-%{upstream_version}
%patch -p1 -P 0
%patch -p1 -P 1
%patch -p1 -P 2
%patch -p1 -P 3
%patch -p1 -P 4
%patch -p1 -P 5
%patch -p1 -P 6
%patch -p1 -P 7
%patch -p1 -P 8
%patch -p1 -P 9
%patch -p1 -P 10
%patch -p1 -P 11
%patch -p1 -P 12
%patch -p1 -P 13
%patch -p1 -P 14
%patch -p1 -P 15
%patch -p1 -P 16
%patch -p1 -P 17

# bundled ha-cloud-support libs
%patch -p1 -P 500
%patch -p1 -P 501

chmod 755 heartbeat/pgsqlms

%build
sed -i -e "s/#PYTHON3_VERSION#/%{python3_version}/" heartbeat/gcp*

if [ ! -f configure ]; then
	./autogen.sh
fi

%if 0%{?fedora} >= 11 || 0%{?centos} > 5 || 0%{?rhel} > 5
CFLAGS="$(echo '%{optflags}')"
%global conf_opt_fatal "--enable-fatal-warnings=no"
%else
CFLAGS="${CFLAGS} ${RPM_OPT_FLAGS}"
%global conf_opt_fatal "--enable-fatal-warnings=yes"
%endif

%if %{with rgmanager}
%global rasset rgmanager
%endif
%if %{with linuxha}
%global rasset linux-ha
%endif
%if %{with rgmanager} && %{with linuxha}
%global rasset all
%endif

export CFLAGS

%configure \
%if 0%{?fedora} || 0%{?centos} > 7 || 0%{?rhel} > 7 || 0%{?suse_version}
	PYTHON="%{__python3}" \
%endif
%ifarch x86_64
	PYTHONPATH="%{_usr}/lib/fence-agents/support/google/lib/python%{python3_version}/site-packages" \
%endif
	%{conf_opt_fatal} \
%if %{defined _unitdir}
    SYSTEMD_UNIT_DIR=%{_unitdir} \
%endif
%if %{defined _tmpfilesdir}
    SYSTEMD_TMPFILES_DIR=%{_tmpfilesdir} \
    --with-rsctmpdir=/run/resource-agents \
%endif
	--with-pkg-name=%{name} \
	--with-ras-set=%{rasset}

make %{_smp_mflags}

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

## tree fixup
# remove docs (there is only one and they should come from doc sections in files)
rm -rf %{buildroot}/usr/share/doc/resource-agents

%files
%doc AUTHORS COPYING COPYING.GPLv3 COPYING.LGPL ChangeLog
%if %{with linuxha}
%doc heartbeat/README.galera
%doc doc/README.webapps
%doc %{_datadir}/%{name}/ra-api-1.dtd
%doc %{_datadir}/%{name}/metadata.rng
%endif

%if %{with rgmanager}
%{_datadir}/cluster
%{_sbindir}/rhev-check.sh
%endif

%if %{with linuxha}
%dir %{_usr}/lib/ocf
%dir %{_usr}/lib/ocf/resource.d
%dir %{_usr}/lib/ocf/lib

%{_usr}/lib/ocf/lib/heartbeat

%{_usr}/lib/ocf/resource.d/heartbeat

%{_datadir}/pkgconfig/%{name}.pc

%if %{defined _unitdir}
%{_unitdir}/resource-agents-deps.target
%endif
%if %{defined _tmpfilesdir}
%{_tmpfilesdir}/%{name}.conf
%endif

%dir %{_datadir}/%{name}
%dir %{_datadir}/%{name}/ocft
%{_datadir}/%{name}/ocft/configs
%{_datadir}/%{name}/ocft/caselib
%{_datadir}/%{name}/ocft/README
%{_datadir}/%{name}/ocft/README.zh_CN
%{_datadir}/%{name}/ocft/helpers.sh
%exclude %{_datadir}/%{name}/ocft/runocft
%exclude %{_datadir}/%{name}/ocft/runocft.prereq

%{_sbindir}/ocf-tester
%{_sbindir}/ocft

%{_includedir}/heartbeat

%if %{defined _tmpfilesdir}
%dir %attr (1755, root, root)	/run/resource-agents
%else
%dir %attr (1755, root, root)	%{_var}/run/resource-agents
%endif

%{_mandir}/man7/*.7*
%{_mandir}/man8/ocf-tester.8*

###
# Supported, but in another sub package
###
%exclude /usr/lib/ocf/resource.d/heartbeat/aliyun-vpc-move-ip*
%exclude /usr/lib/ocf/resource.d/heartbeat/aws*
%exclude /usr/lib/ocf/resource.d/heartbeat/azure-*
%exclude %{_mandir}/man7/*aliyun-vpc-move-ip*
%exclude /usr/lib/ocf/resource.d/heartbeat/gcp*
%exclude %{_mandir}/man7/*gcp*
%exclude /usr/lib/ocf/resource.d/heartbeat/powervs-subnet
%exclude %{_mandir}/man7/*powervs-subnet*
%exclude /usr/lib/ocf/resource.d/heartbeat/pgsqlms
%exclude %{_mandir}/man7/*pgsqlms*
%exclude %{_usr}/lib/ocf/lib/heartbeat/OCF_*.pm

###
# Moved to separate packages
###
%exclude /usr/lib/ocf/resource.d/heartbeat/SAP*
%exclude /usr/lib/ocf/lib/heartbeat/sap*
%exclude %{_mandir}/man7/*SAP*

###
# Unsupported
###
%exclude %{_usr}/lib/ocf/resource.d/heartbeat/AoEtarget
%exclude %{_usr}/lib/ocf/resource.d/heartbeat/AudibleAlarm
%exclude %{_usr}/lib/ocf/resource.d/heartbeat/ClusterMon
%exclude %{_usr}/lib/ocf/resource.d/heartbeat/EvmsSCC
%exclude %{_usr}/lib/ocf/resource.d/heartbeat/Evmsd
%exclude %{_usr}/lib/ocf/resource.d/heartbeat/ICP
%exclude %{_usr}/lib/ocf/resource.d/heartbeat/IPaddr
%exclude %{_usr}/lib/ocf/resource.d/heartbeat/LVM
%exclude %{_usr}/lib/ocf/resource.d/heartbeat/LinuxSCSI
%exclude %{_usr}/lib/ocf/resource.d/heartbeat/ManageRAID
%exclude %{_usr}/lib/ocf/resource.d/heartbeat/ManageVE
%exclude %{_usr}/lib/ocf/resource.d/heartbeat/Pure-FTPd
%exclude %{_usr}/lib/ocf/resource.d/heartbeat/Raid1
%exclude %{_usr}/lib/ocf/resource.d/heartbeat/ServeRAID
%exclude %{_usr}/lib/ocf/resource.d/heartbeat/SphinxSearchDaemon
%exclude %{_usr}/lib/ocf/resource.d/heartbeat/Stateful
%exclude %{_usr}/lib/ocf/resource.d/heartbeat/SysInfo
%exclude %{_usr}/lib/ocf/resource.d/heartbeat/VIPArip
%exclude %{_usr}/lib/ocf/resource.d/heartbeat/WAS
%exclude %{_usr}/lib/ocf/resource.d/heartbeat/WAS6
%exclude %{_usr}/lib/ocf/resource.d/heartbeat/WinPopup
%exclude %{_usr}/lib/ocf/resource.d/heartbeat/Xen
%exclude %{_usr}/lib/ocf/resource.d/heartbeat/ZFS
%exclude %{_usr}/lib/ocf/resource.d/heartbeat/anything
%exclude %{_usr}/lib/ocf/resource.d/heartbeat/asterisk
%exclude %{_usr}/lib/ocf/resource.d/heartbeat/clvm
%exclude %{_usr}/lib/ocf/resource.d/heartbeat/dnsupdate
%exclude %{_usr}/lib/ocf/resource.d/heartbeat/docker*
%exclude %{_usr}/lib/ocf/resource.d/heartbeat/dovecot
%exclude %{_usr}/lib/ocf/resource.d/heartbeat/dummypy
%exclude %{_usr}/lib/ocf/resource.d/heartbeat/eDir88
%exclude %{_usr}/lib/ocf/resource.d/heartbeat/fio
%exclude %{_usr}/lib/ocf/resource.d/heartbeat/ids
%exclude %{_usr}/lib/ocf/resource.d/heartbeat/iface-bridge
%exclude %{_usr}/lib/ocf/resource.d/heartbeat/iface-macvlan
%exclude %{_usr}/lib/ocf/resource.d/heartbeat/ipsec
%exclude %{_usr}/lib/ocf/resource.d/heartbeat/iscsi
%exclude %{_usr}/lib/ocf/resource.d/heartbeat/jboss
%exclude %{_usr}/lib/ocf/resource.d/heartbeat/jira
%exclude %{_usr}/lib/ocf/resource.d/heartbeat/kamailio
%exclude %{_usr}/lib/ocf/resource.d/heartbeat/ldirectord
%exclude %{_usr}/lib/ocf/resource.d/heartbeat/lxc
%exclude %{_usr}/lib/ocf/resource.d/heartbeat/lxd-info
%exclude %{_usr}/lib/ocf/resource.d/heartbeat/machine-info
%exclude %{_usr}/lib/ocf/resource.d/heartbeat/mariadb
%exclude %{_usr}/lib/ocf/resource.d/heartbeat/mdraid
%exclude %{_usr}/lib/ocf/resource.d/heartbeat/minio
%exclude %{_usr}/lib/ocf/resource.d/heartbeat/mpathpersist
%exclude %{_usr}/lib/ocf/resource.d/heartbeat/mysql-proxy
%exclude %{_usr}/lib/ocf/resource.d/heartbeat/nvmet-*
%exclude %{_usr}/lib/ocf/resource.d/heartbeat/ocivip
%exclude %{_usr}/lib/ocf/resource.d/heartbeat/osceip
%exclude %{_usr}/lib/ocf/resource.d/heartbeat/ovsmonitor
%exclude %{_usr}/lib/ocf/resource.d/heartbeat/pgagent
%exclude %{_usr}/lib/ocf/resource.d/heartbeat/pingd
%exclude %{_usr}/lib/ocf/resource.d/heartbeat/pound
%exclude %{_usr}/lib/ocf/resource.d/heartbeat/proftpd
%exclude %{_usr}/lib/ocf/resource.d/heartbeat/rabbitmq-server-ha
%exclude %{_usr}/lib/ocf/resource.d/heartbeat/rkt
%exclude %{_usr}/lib/ocf/resource.d/heartbeat/rsyslog
%exclude %{_usr}/lib/ocf/resource.d/heartbeat/scsi2reservation
%exclude %{_usr}/lib/ocf/resource.d/heartbeat/sfex
%exclude %{_usr}/lib/ocf/resource.d/heartbeat/sg_persist
%exclude %{_usr}/lib/ocf/resource.d/heartbeat/smb-share
%exclude %{_usr}/lib/ocf/resource.d/heartbeat/syslog-ng
%exclude %{_usr}/lib/ocf/resource.d/heartbeat/varnish
%exclude %{_usr}/lib/ocf/resource.d/heartbeat/vmware
%exclude %{_usr}/lib/ocf/resource.d/heartbeat/vsftpd
%exclude %{_usr}/lib/ocf/resource.d/heartbeat/zabbixserver
%exclude %{_mandir}/man7/ocf_heartbeat_AoEtarget.7.gz
%exclude %{_mandir}/man7/ocf_heartbeat_AudibleAlarm.7.gz
%exclude %{_mandir}/man7/ocf_heartbeat_ClusterMon.7.gz
%exclude %{_mandir}/man7/ocf_heartbeat_EvmsSCC.7.gz
%exclude %{_mandir}/man7/ocf_heartbeat_Evmsd.7.gz
%exclude %{_mandir}/man7/ocf_heartbeat_ICP.7.gz
%exclude %{_mandir}/man7/ocf_heartbeat_IPaddr.7.gz
%exclude %{_mandir}/man7/ocf_heartbeat_LVM.7.gz
%exclude %{_mandir}/man7/ocf_heartbeat_LinuxSCSI.7.gz
%exclude %{_mandir}/man7/ocf_heartbeat_ManageRAID.7.gz
%exclude %{_mandir}/man7/ocf_heartbeat_ManageVE.7.gz
%exclude %{_mandir}/man7/ocf_heartbeat_Pure-FTPd.7.gz
%exclude %{_mandir}/man7/ocf_heartbeat_Raid1.7.gz
%exclude %{_mandir}/man7/ocf_heartbeat_ServeRAID.7.gz
%exclude %{_mandir}/man7/ocf_heartbeat_SphinxSearchDaemon.7.gz
%exclude %{_mandir}/man7/ocf_heartbeat_Stateful.7.gz
%exclude %{_mandir}/man7/ocf_heartbeat_SysInfo.7.gz
%exclude %{_mandir}/man7/ocf_heartbeat_VIPArip.7.gz
%exclude %{_mandir}/man7/ocf_heartbeat_WAS.7.gz
%exclude %{_mandir}/man7/ocf_heartbeat_WAS6.7.gz
%exclude %{_mandir}/man7/ocf_heartbeat_WinPopup.7.gz
%exclude %{_mandir}/man7/ocf_heartbeat_Xen.7.gz
%exclude %{_mandir}/man7/ocf_heartbeat_ZFS.7.gz
%exclude %{_mandir}/man7/ocf_heartbeat_anything.7.gz
%exclude %{_mandir}/man7/ocf_heartbeat_asterisk.7.gz
%exclude %{_mandir}/man7/ocf_heartbeat_clvm.7.gz
%exclude %{_mandir}/man7/ocf_heartbeat_dnsupdate.7.gz
%exclude %{_mandir}/man7/ocf_heartbeat_docker*.7.gz
%exclude %{_mandir}/man7/ocf_heartbeat_dovecot.7.gz
%exclude %{_mandir}/man7/ocf_heartbeat_dummypy.7.gz
%exclude %{_mandir}/man7/ocf_heartbeat_eDir88.7.gz
%exclude %{_mandir}/man7/ocf_heartbeat_fio.7.gz
%exclude %{_mandir}/man7/ocf_heartbeat_ids.7.gz
%exclude %{_mandir}/man7/ocf_heartbeat_iface-bridge.7.gz
%exclude %{_mandir}/man7/ocf_heartbeat_iface-macvlan.7.gz
%exclude %{_mandir}/man7/ocf_heartbeat_ipsec.7.gz
%exclude %{_mandir}/man7/ocf_heartbeat_iscsi.7.gz
%exclude %{_mandir}/man7/ocf_heartbeat_jboss.7.gz
%exclude %{_mandir}/man7/ocf_heartbeat_jira.7.gz
%exclude %{_mandir}/man7/ocf_heartbeat_kamailio.7.gz
%exclude %{_mandir}/man7/ocf_heartbeat_lxc.7.gz
%exclude %{_mandir}/man7/ocf_heartbeat_lxd-info.7.gz
%exclude %{_mandir}/man7/ocf_heartbeat_machine-info.7.gz
%exclude %{_mandir}/man7/ocf_heartbeat_mariadb.7.gz
%exclude %{_mandir}/man7/ocf_heartbeat_mdraid.7.gz
%exclude %{_mandir}/man7/ocf_heartbeat_minio.7.gz
%exclude %{_mandir}/man7/ocf_heartbeat_mpathpersist.7.gz
%exclude %{_mandir}/man7/ocf_heartbeat_mysql-proxy.7.gz
%exclude %{_mandir}/man7/ocf_heartbeat_nvmet-*.7.gz
%exclude %{_mandir}/man7/ocf_heartbeat_ocivip.7.gz
%exclude %{_mandir}/man7/ocf_heartbeat_osceip.7.gz
%exclude %{_mandir}/man7/ocf_heartbeat_ovsmonitor.7.gz
%exclude %{_mandir}/man7/ocf_heartbeat_pgagent.7.gz
%exclude %{_mandir}/man7/ocf_heartbeat_pingd.7.gz
%exclude %{_mandir}/man7/ocf_heartbeat_pound.7.gz
%exclude %{_mandir}/man7/ocf_heartbeat_proftpd.7.gz
%exclude %{_mandir}/man7/ocf_heartbeat_rabbitmq-server-ha.7.gz
%exclude %{_mandir}/man7/ocf_heartbeat_rkt.7.gz
%exclude %{_mandir}/man7/ocf_heartbeat_rsyslog.7.gz
%exclude %{_mandir}/man7/ocf_heartbeat_scsi2reservation.7.gz
%exclude %{_mandir}/man7/ocf_heartbeat_sfex.7.gz
%exclude %{_mandir}/man7/ocf_heartbeat_sg_persist.7.gz
%exclude %{_mandir}/man7/ocf_heartbeat_smb-share.7.gz
%exclude %{_mandir}/man7/ocf_heartbeat_syslog-ng.7.gz
%exclude %{_mandir}/man7/ocf_heartbeat_varnish.7.gz
%exclude %{_mandir}/man7/ocf_heartbeat_vmware.7.gz
%exclude %{_mandir}/man7/ocf_heartbeat_vsftpd.7.gz
%exclude %{_mandir}/man7/ocf_heartbeat_zabbixserver.7.gz

###
# Other excluded files.
###
# ldirectord is not supported
%exclude /etc/ha.d/resource.d/ldirectord
%exclude %{_sysconfdir}/rc.d/init.d/ldirectord
%exclude %{_unitdir}/ldirectord.service
%exclude /etc/logrotate.d/ldirectord
%exclude /usr/sbin/ldirectord
%exclude %{_mandir}/man8/ldirectord.8.gz

# For compatability with pre-existing agents
%dir %{_sysconfdir}/ha.d
%{_sysconfdir}/ha.d/shellfuncs

%{_libexecdir}/heartbeat
%endif

%ifarch x86_64
%files cloud
/usr/lib/ocf/resource.d/heartbeat/aliyun-*
%{_mandir}/man7/*aliyun-*
/usr/lib/ocf/resource.d/heartbeat/aws*
%{_mandir}/man7/*aws*
/usr/lib/ocf/resource.d/heartbeat/azure-*
%{_mandir}/man7/*azure-*
/usr/lib/ocf/resource.d/heartbeat/gcp-*
%{_mandir}/man7/*gcp-*
/usr/lib/ocf/resource.d/heartbeat/powervs-subnet
%{_mandir}/man7/*powervs-subnet*
%exclude /usr/lib/ocf/resource.d/heartbeat/azure-events
%exclude %{_mandir}/man7/*azure-events.7*
%exclude /usr/lib/ocf/resource.d/heartbeat/gcp-vpc-move-ip
%exclude %{_mandir}/man7/*gcp-vpc-move-ip*
%endif

%files paf
%doc paf_README.md
%license paf_LICENSE
%defattr(-,root,root)
%{_usr}/lib/ocf/resource.d/heartbeat/pgsqlms
%{_mandir}/man7/*pgsqlms*
%{_usr}/lib/ocf/lib/heartbeat/OCF_*.pm

%changelog
* Thu Feb 20 2025 Oyvind Albrigtsen <oalbrigt@redhat.com> - 4.16.0-9
- portblock: fix iptables version detection
- Remove unsupported agents

  Resolves: RHEL-79822, RHEL-80293


* Tue Feb 11 2025 Oyvind Albrigtsen <oalbrigt@redhat.com> - 4.16.0-8
- ocf-shellfuncs: fix syntax error in crm_mon_no_validation()

  Resolves: RHEL-73689

* Mon Jan 27 2025 Oyvind Albrigtsen <oalbrigt@redhat.com> - 4.16.0-7
- storage-mon: fix daemon mode bug that caused delayed initial score

  Resolves: RHEL-76037

* Thu Jan 23 2025 Oyvind Albrigtsen <oalbrigt@redhat.com> - 4.16.0-3
- openstack-cinder-volume: wait for volume to be available
- All agents: use grep -E/-F
- Filesystem: dont report warnings when creating resource

  Resolves: RHEL-72954, RHEL-75574, RHEL-76007

* Tue Nov 26 2024 Oyvind Albrigtsen <oalbrigt@redhat.com> - 4.16.0-1
- Rebase to resource-agents 4.16.0 upstream release
- AWS agents: reuse IMDS token until it expires
- awsvip: add interface parameter
- ethmonitor: add bc dependency
- build: use /usr/sbin path for nfs-utils dependencies

  Resolves: RHEL-65331, RHEL-66293, RHEL-68740, RHEL-53615, RHEL-68840

* Tue Oct 29 2024 Troy Dawson <tdawson@redhat.com> - 4.15.1-1.1
- Bump release for October 2024 mass rebuild:
  Resolves: RHEL-64018

* Fri Jul 26 2024 Oyvind Albrigtsen <oalbrigt@redhat.com> - 4.15.1-1
- Rebase to resource-agents 4.15.1 upstream release
- IPaddr2: change default for lvs_ipv6_addrlabel to true to avoid
  last added IP becoming src IP
- powervs-subnet: new resource agent

  Resolves: RHEL-50378, RHEL-46557, RHEL-50380

* Thu Jun 27 2024 Oyvind Albrigtsen <oalbrigt@redhat.com> - 4.13.0-6
- apache: prefer curl due to wget2 issues, and dont use -L for wget2

  Resolves: RHEL-40720

* Mon Jun 24 2024 Troy Dawson <tdawson@redhat.com> - 4.13.0-4.1
- Bump release for June 2024 mass rebuild

* Wed Jun 12 2024 Oyvind Albrigtsen <oalbrigt@redhat.com> - 4.13.0-4
- cloud agents: set support library path
- pgsqlms: add to -paf subpackage

* Tue Jan 30 2024 Zbigniew Jedrzejewski-Szmek <zbyszek@in.waw.pl> - 4.13.0-2.3
- Replace /sbin by /usr/sbin in some paths so that the package remains
  installable without full filepath metadata (rhbz#2229951)

* Fri Jan 26 2024 Fedora Release Engineering <releng@fedoraproject.org> - 4.13.0-2.2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_40_Mass_Rebuild

* Mon Jan 22 2024 Fedora Release Engineering <releng@fedoraproject.org> - 4.13.0-2.1
- Rebuilt for https://fedoraproject.org/wiki/Fedora_40_Mass_Rebuild

* Wed Jan 10 2024 Oyvind Albrigtsen <oalbrigt@redhat.com> - 4.13.0-2
- configure: fix "C preprocessor "gcc -E" fails sanity check" error
  with autoconf 2.72+

  Resolves: rhbz#2256836

* Wed Oct 11 2023 Oyvind Albrigtsen <oalbrigt@redhat.com> - 4.13.0-1
- Rebase to resource-agents 4.13.0 upstream release.
