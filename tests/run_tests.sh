#!/bin/sh

err=0

agents=$(pcs resource list ocf:heartbeat 2>&1)
if [ $(echo "$agents" | wc -l) -lt 2 ]; then
	echo "ERROR: pcs: agents available:\n$agents"
	err=$((err+1))
else
	echo "INFO: pcs: agents available..."
fi

for bin in "/usr/lib/fence-agents/support/aliyun/aliyun-cli/aliyun version"; do
	output="$($bin 2>&1)"
	if [ $? -ne 0 ]; then
		echo -e "ERROR: $bin failed:\n$output"
		err=$((err+1))
	else
		echo "INFO: $bin works..."
	fi
done

if [ $err -ge 1 ]; then
	echo -e "\nERROR: $err tests FAILED..."
	exit 1
fi
